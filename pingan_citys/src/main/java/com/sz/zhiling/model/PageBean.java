package com.sz.zhiling.model;

import java.util.List;

//自定义的通用的分页类；
public class PageBean<T> {
    private long total;   //所有记录行数；
    private List<T> rows;  //当前页的数据；
    private int pageSize = 5; //每页5条；
    private int pageTotals = 0; //一共多少页；
    private int pageIndex = 1;//第几页；

    public PageBean() {
    }

    public PageBean(long total, List<T> rows, int pageSize, int pageTotals, int pageIndex) {
        this.total = total;
        this.rows = rows;
        this.pageSize = pageSize;
        this.pageTotals = pageTotals;
        this.pageIndex = pageIndex;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageTotals() {
        return pageTotals;
    }

    public void setPageTotals(int pageTotals) {
        this.pageTotals = pageTotals;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }
}
