package com.sz.zhiling.mapper;

import com.sz.zhiling.model.TUsers;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface TUsersMapper extends Mapper<TUsers> {

    //只需要继承Mapper，剩下的事件交给通用Mapper完成！！！！！


}