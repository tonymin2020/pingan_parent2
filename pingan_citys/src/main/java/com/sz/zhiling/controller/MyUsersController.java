package com.sz.zhiling.controller;

import com.sz.zhiling.model.PageBean;
import com.sz.zhiling.model.TUsers;
import com.sz.zhiling.service.IMyUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/users")
public class MyUsersController {


    @Autowired
    private IMyUserService iMyUserService;


    @GetMapping("/list")
    @ResponseBody
    public PageBean<TUsers> list(Model model, @RequestParam(required = true, defaultValue = "1") int index){

        return iMyUserService.getAllUsers(index);
    }
}
