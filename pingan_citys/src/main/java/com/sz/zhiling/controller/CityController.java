package com.sz.zhiling.controller;

import com.sz.zhiling.model.PageBean;
import com.sz.zhiling.model.Result;
import com.sz.zhiling.model.TCity;
import com.sz.zhiling.service.ICitySerivce;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController                  //返回全是json;
@RequestMapping("/citys")
public class CityController {


    @Autowired
    private ICitySerivce iCitySerivce;


    //0.无分页查询所有城市；
    @GetMapping("/list")
    public PageBean<TCity> getAllCity(){
         return iCitySerivce.getAllCity(1,5);
    }

    //1.带分页查询所有城市；
    @GetMapping("/getAllByPages")
    public PageBean<TCity> getAllByPages(int index, int size){
        return iCitySerivce.getAllCity(index,size);
    }


    //2.查询所有的省；
    @GetMapping("/getAllProvince")
    public List<TCity>  getAllProvinces(){
        return iCitySerivce.getAllProvinces();
    }


    //3.带条件的综合查询：  TCity tCity   ;   @RequestBody ：把前端传入的json: {"cId":"0","cName":"州","fId":19}  转为tcity
    @PostMapping("/searchAll")
    public PageBean<TCity> searchAll(int index, int size,@RequestBody TCity tCity){
        System.out.println("tCity.toString() = " + tCity.toString());
        System.out.println("index = " + index);
        System.out.println("size : = " + size);
        System.out.println("size : = " + size);
        return iCitySerivce.searchAllCitys(index,size,tCity);
    }

    //4.删除一个城市；
    @GetMapping("/delete")
    public PageBean<TCity> delete(int cId){
        iCitySerivce.deleteCity(cId);
        return iCitySerivce.searchAllCitys(1,5,null);
    }


    @PostMapping("/save")
    public Result save(@RequestBody TCity tCity){
        System.out.println(tCity);
        Result result = null;
        if (tCity.getcId()!=null && !"".equals(tCity.getcId())){
            //修改操作；
            iCitySerivce.updateCity(tCity);
            result = new Result(0,"修改成功！");
        }else {
            //添加操作；
            iCitySerivce.addCity(tCity);
            result = new Result(0, "添加成功！");
        }
        return result;
    }


    //查询一个城市；
    @GetMapping("/getCityByCid")
    public TCity getCityByCid(int cId){
        return iCitySerivce.getCityByCid(cId);

    }

}
