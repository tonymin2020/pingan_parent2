package com.sz.zhiling.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sz.zhiling.mapper.TUsersMapper;
import com.sz.zhiling.model.PageBean;
import com.sz.zhiling.model.TUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyUserSerivceImpl implements IMyUserService {


    @Autowired
    private TUsersMapper tUsersMapper;

    @Override
    public PageBean<TUsers> getAllUsers(int index) {
        //开始分页； 默认每页2条数据；
        PageHelper.startPage(index,2);
        List<TUsers> rows = tUsersMapper.selectAll();
        Page<TUsers> plist = (Page<TUsers>) rows;
        PageBean<TUsers> pb = new PageBean<>(
                plist.getTotal(),
                plist.getResult(),
                plist.getPageSize(),
                plist.getPages(),
                plist.getPageNum()
        );
        return pb;
    }
}
