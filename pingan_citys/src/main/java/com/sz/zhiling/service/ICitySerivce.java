package com.sz.zhiling.service;

import com.sz.zhiling.model.PageBean;
import com.sz.zhiling.model.TCity;

import java.util.List;

public interface ICitySerivce {


    public PageBean<TCity> getAllCity(int index, int size);

    void deleteCity(int cid);

    public PageBean<TCity> searchAllCitys(int i, int i1, TCity tCity);

    public List<TCity> getAllProvinces();

    void updateCity(TCity tCity);

    void addCity(TCity tCity);

    TCity getCityByCid(int cId);
}
