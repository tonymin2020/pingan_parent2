package com.sz.zhiling.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sz.zhiling.mapper.TCityMapper;
import com.sz.zhiling.model.PageBean;
import com.sz.zhiling.model.TCity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class CityServiceImpl implements ICitySerivce {

    @Autowired
    private TCityMapper tCityMapper;


    @Override
    public PageBean<TCity> getAllCity(int index, int size) {
        PageHelper.startPage(index,size);  //开始分页；
        List<TCity> list = tCityMapper.selectAll();   //不带查询条件，就是查询所有；
        Page<TCity> plist = (Page<TCity>) list;
        PageBean<TCity> pb = new PageBean<>(
                plist.getTotal(),
                plist.getResult(),
                plist.getPageSize(),
                plist.getPages(),
                plist.getPageNum()

        );
        return pb;
    }

    @Override
    public void deleteCity(int cid) {
        tCityMapper.deleteByPrimaryKey(cid);
    }

    @Override
    public PageBean<TCity> searchAllCitys(int i, int i1, TCity tCity) {
        PageHelper.startPage(i,i1);  //开始分页；
        Example example = new Example(TCity.class);
        Example.Criteria criteria = example.createCriteria();
        if (tCity!=null){
            if (tCity.getcId()!=null && tCity.getcId()>0){
                criteria.andEqualTo("cId",tCity.getcId());        //带查询条件，精准查询；
            }
            if (tCity.getfId()!=null && tCity.getfId()>0){
                criteria.andEqualTo("fId",tCity.getfId());        //带查询条件，精准查询；
            }
            if (null!=tCity.getcName() && !"".equals(tCity.getcName())){
                criteria.andLike("cName","%"+tCity.getcName()+"%"); //带查询条件，模糊查询；
            }
        }
        List<TCity> list = tCityMapper.selectByExample(example);
        Page<TCity> plist = (Page<TCity>) list;
        PageBean<TCity> pb = new PageBean<>(
                plist.getTotal(),
                plist.getResult(),
                plist.getPageSize(),
                plist.getPages(),
                plist.getPageNum()

        );
        return pb;
    }

    @Override
    public List<TCity> getAllProvinces() {
        //查询所有的省；  select * from t_city where fid=0;

        Example example = new Example(TCity.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("fId",0);
        return tCityMapper.selectByExample(example);
    }

    @Override
    public void updateCity(TCity tCity) {
        tCityMapper.updateByPrimaryKey(tCity);
    }

    @Override
    public void addCity(TCity tCity) {
          tCityMapper.insert(tCity);
    }

    @Override
    public TCity getCityByCid(int cId) {
        return tCityMapper.selectByPrimaryKey(cId);
    }
}
