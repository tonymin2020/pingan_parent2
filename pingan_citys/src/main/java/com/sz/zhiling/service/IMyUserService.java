package com.sz.zhiling.service;

import com.sz.zhiling.model.PageBean;
import com.sz.zhiling.model.TUsers;

public interface IMyUserService {

    //1.定义一个查询所有的用户接口；
    public PageBean<TUsers> getAllUsers(int index);
}
