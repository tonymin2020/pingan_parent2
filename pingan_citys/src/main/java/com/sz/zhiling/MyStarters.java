package com.sz.zhiling;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.sz.zhiling.mapper")
@EnableEurekaClient    //启动客户端；
public class MyStarters {

    public static void main(String[] args) {
        //程序入口处，一键启动！！！
        SpringApplication.run(MyStarters.class);
    }
}
