package com.sz.zhiling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient   //启用注册客户端；
@EnableFeignClients       //启用微服务调用；
public class UserStarers {
    public static void main(String[] args) {
        SpringApplication.run(UserStarers.class);
    }
}
