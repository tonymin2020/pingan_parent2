package com.sz.zhiling.controller;

import com.sz.zhiling.client.CitysClient;
import com.sz.zhiling.model.MyUsers;
import com.sz.zhiling.model.PageBean;
import com.sz.zhiling.model.TCity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class MyUsersController {

    @Autowired
    private CitysClient citysClient;

    //调用所有的用户；
    @RequestMapping("/list")
    public List<MyUsers> getAllUsers(){
        List<MyUsers> list = new ArrayList<>();
        list.add(new MyUsers(1,"admin","123"));
        list.add(new MyUsers(2,"tom","1234"));
        list.add(new MyUsers(3,"jack","1235"));
        list.add(new MyUsers(4,"rose","1236"));
        return list;
    }


    //调用pingan-citys中所有的城市的方法；
    @RequestMapping("/citys/list")
    public PageBean<TCity> getAllCity(){
        return citysClient.getAllCity();
    }
}
