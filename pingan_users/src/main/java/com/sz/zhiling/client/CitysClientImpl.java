package com.sz.zhiling.client;

import com.sz.zhiling.model.PageBean;
import com.sz.zhiling.model.TCity;
import org.springframework.stereotype.Component;

@Component
public class CitysClientImpl implements CitysClient {
    @Override
    public PageBean<TCity> getAllCity() {
        System.out.println("pingan-citys服务已经宕机了，所以pingan-users中的熔断器起作用了！");
        return new PageBean<>();
    }
}
