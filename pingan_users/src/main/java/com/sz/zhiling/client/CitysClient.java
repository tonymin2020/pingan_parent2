package com.sz.zhiling.client;

import com.sz.zhiling.model.PageBean;
import com.sz.zhiling.model.TCity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "pingan-citys", fallback = CitysClientImpl.class)   //如果pingan-citys中的功能雪崩，则自我熔断！
public interface CitysClient {

    //这是我在gitee上修改的代码。。。。。


    //这是我在idea工具中修改的代码注释
    @GetMapping("/citys/list")
    public PageBean<TCity> getAllCity();
}
